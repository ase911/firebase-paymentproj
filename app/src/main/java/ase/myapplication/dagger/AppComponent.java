package ase.myapplication.dagger;

import javax.inject.Singleton;

import ase.myapplication.presentationlayer.authorizationview.view.AuthorizationActivity;
import ase.myapplication.presentationlayer.mainview.view.MainActivity;
import ase.myapplication.presentationlayer.showdataview.view.ShowActivity;
import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, MainModule.class})
public interface AppComponent {
    void inject(MainActivity mainActivity);
    void inject(AuthorizationActivity auth);
    void inject(ShowActivity showActivity);
}
