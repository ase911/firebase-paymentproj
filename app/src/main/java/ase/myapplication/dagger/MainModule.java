package ase.myapplication.dagger;

import ase.myapplication.busineslayer.authorizationview.interactor.AuthorizationInteractor;
import ase.myapplication.busineslayer.authorizationview.interactor.AuthorizationInteractorImpl;
import ase.myapplication.busineslayer.mainview.interactor.MainInteractor;
import ase.myapplication.busineslayer.mainview.interactor.MainInteractorImpl;
import ase.myapplication.busineslayer.showview.showinteractor.ShowInteractor;
import ase.myapplication.busineslayer.showview.showinteractor.ShowInteractorImpl;
import dagger.Binds;
import dagger.Module;
import dagger.Reusable;

@Module
abstract class MainModule {
    @Binds
    @Reusable
    abstract MainInteractor mainInteractor(MainInteractorImpl mainInteractor);

    @Binds
    @Reusable
    abstract AuthorizationInteractor authInteractor(AuthorizationInteractorImpl authInteractor);

    @Binds
    @Reusable
    abstract ShowInteractor showInteractor(ShowInteractorImpl showInteractor);

}
