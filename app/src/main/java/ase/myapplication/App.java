package ase.myapplication;

import android.app.Application;

import ase.myapplication.dagger.AppComponent;
import ase.myapplication.dagger.AppModule;
import ase.myapplication.dagger.DaggerAppComponent;

/**
 * Created by ase911 on 15.06.2017.
 */

public class App extends Application {

    private static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = buildComponent();
    }

    public static AppComponent getComponent(){return component;}

    private AppComponent buildComponent(){
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }
}