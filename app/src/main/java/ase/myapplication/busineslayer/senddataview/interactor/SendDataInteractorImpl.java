package ase.myapplication.busineslayer.senddataview.interactor;

import ase.myapplication.datalayer.firebaseservices.RealtimeDatabaseService;

public class SendDataInteractorImpl implements SendDataInteractor {

    private RealtimeDatabaseService realtimeDatabaseService = new RealtimeDatabaseService();
    @Override
    public void putIntoDB(String s, String s1) {
        realtimeDatabaseService.putIntoDB(s, s1);
    }
}
