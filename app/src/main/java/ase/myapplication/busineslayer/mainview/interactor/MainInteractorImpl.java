package ase.myapplication.busineslayer.mainview.interactor;

import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

import ase.myapplication.datalayer.firebaseservices.FirebaseAuthorizationService;
import dagger.Reusable;
import io.reactivex.Completable;

@Reusable
public class MainInteractorImpl implements MainInteractor {

    private FirebaseAuthorizationService firebaseAuthorizationService;

    @Inject
    MainInteractorImpl(FirebaseAuthorizationService firebaseAuthorizationService) {
        this.firebaseAuthorizationService = firebaseAuthorizationService;
    }
//    @Override
//    public void GetAllAboutUser() {
//
//    }

    @Override
    public void FirebaseSignOut() {
        firebaseAuthorizationService.signOut();
    }

    @Override
    public Completable VerifyEmail(FirebaseUser currentUser) {

        return firebaseAuthorizationService.verification(currentUser);
    }


}
