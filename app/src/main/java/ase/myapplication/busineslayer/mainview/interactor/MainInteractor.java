package ase.myapplication.busineslayer.mainview.interactor;

import com.google.firebase.auth.FirebaseUser;

import io.reactivex.Completable;

public interface MainInteractor {

//    void GetAllAboutUser();

    void FirebaseSignOut();

    Completable VerifyEmail(FirebaseUser currentUser);

}
