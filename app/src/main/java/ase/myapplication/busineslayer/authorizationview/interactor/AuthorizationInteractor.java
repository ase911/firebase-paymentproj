package ase.myapplication.busineslayer.authorizationview.interactor;

import com.google.firebase.auth.FirebaseAuth;

import io.reactivex.Completable;

public interface AuthorizationInteractor {

    Completable FirebaseSignIn(FirebaseAuth auth, String s, String s1);

    Completable firebaseRegistration(FirebaseAuth mAuth, String s, String s1);
}
