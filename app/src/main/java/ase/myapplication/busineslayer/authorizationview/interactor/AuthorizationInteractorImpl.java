package ase.myapplication.busineslayer.authorizationview.interactor;

import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

import ase.myapplication.datalayer.firebaseservices.FirebaseAuthorizationService;
import dagger.Reusable;
import io.reactivex.Completable;

@Reusable
public class AuthorizationInteractorImpl implements AuthorizationInteractor {

    private final FirebaseAuthorizationService firebaseAuthorizationService;

    @Inject
    AuthorizationInteractorImpl(FirebaseAuthorizationService firebaseAuthorizationService) {
        this.firebaseAuthorizationService = firebaseAuthorizationService;
    }

    @Override
    public Completable FirebaseSignIn(FirebaseAuth auth, String s, String s1) {
        return firebaseAuthorizationService.signIn(auth, s, s1);
    }

    @Override
    public Completable firebaseRegistration(FirebaseAuth mAuth, String s, String s1) {
        return firebaseAuthorizationService.registration(mAuth, s, s1);
    }
}
