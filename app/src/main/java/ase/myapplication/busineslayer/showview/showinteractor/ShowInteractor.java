package ase.myapplication.busineslayer.showview.showinteractor;

import java.util.List;

import ase.myapplication.datalayer.InputDataModel;

public interface ShowInteractor {
    List<InputDataModel> getDataFromFB();
}
