package ase.myapplication.busineslayer.showview.showinteractor;

import java.util.List;

import javax.inject.Inject;

import ase.myapplication.datalayer.InputDataModel;
import ase.myapplication.datalayer.firebaseservices.RealtimeDatabaseService;
import dagger.Reusable;

@Reusable
public class ShowInteractorImpl implements ShowInteractor {

    private RealtimeDatabaseService realtimeDatabaseService;

    @Inject
    ShowInteractorImpl(RealtimeDatabaseService realtimeDatabaseService) {
        this.realtimeDatabaseService = realtimeDatabaseService;
    }

    @Override
    public List<InputDataModel> getDataFromFB() {
        return realtimeDatabaseService.getDataForFB();
    }
}
