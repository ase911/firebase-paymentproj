package ase.myapplication.presentationlayer.authorizationview.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

import ase.myapplication.App;
import ase.myapplication.R;
import ase.myapplication.presentationlayer.authorizationview.presenter.AuthorizationPresenter;
import ase.myapplication.presentationlayer.mainview.view.MainActivity;

public class AuthorizationActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEmailField;
    private EditText mPasswordField;

    private FirebaseAuth mAuth;

    @Inject
    protected AuthorizationPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEmailField = (EditText) findViewById(R.id.field_email);
        mPasswordField = (EditText) findViewById(R.id.field_password);
        findViewById(R.id.email_sign_in_button).setOnClickListener(this);
        findViewById(R.id.registration_button).setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
        App.getComponent().inject(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.attachView(this);
        updateUI();
    }

    public void updateUI() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.email_sign_in_button) {
            presenter.SignIn(mAuth, mEmailField.getText().toString(), mPasswordField.getText().toString());
        }else if (i == R.id.registration_button){
            presenter.registryThisUser(mAuth, mEmailField.getText().toString(), mPasswordField.getText().toString());
        }
    }

    public void showToast(String ae) {
        Toast.makeText(this, ae, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStop() {
        presenter.detachView();
        super.onStop();
    }
}
