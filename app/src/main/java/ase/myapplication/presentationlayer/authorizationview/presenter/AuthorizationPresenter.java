package ase.myapplication.presentationlayer.authorizationview.presenter;

import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

import ase.myapplication.busineslayer.authorizationview.interactor.AuthorizationInteractorImpl;
import ase.myapplication.presentationlayer.authorizationview.view.AuthorizationActivity;
import dagger.Reusable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

@Reusable
public class AuthorizationPresenter {

    private final AuthorizationInteractorImpl interactor;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private AuthorizationActivity authorizationActivity;

    @Inject
    AuthorizationPresenter(AuthorizationInteractorImpl interactor) {
        this.interactor = interactor;
    }

    public void SignIn(FirebaseAuth auth, String s, String s1) {
        compositeDisposable
                .add(interactor.FirebaseSignIn(auth, s, s1)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> authorizationActivity.updateUI(),
                                throwable -> authorizationActivity.showToast(throwable.getMessage())));

    }

    public void detachView() {
        compositeDisposable.clear();
        authorizationActivity = null;
    }

    public void attachView(AuthorizationActivity authorizationActivity) {
        this.authorizationActivity = authorizationActivity;
    }


    public void registryThisUser(FirebaseAuth mAuth, String s, String s1) {
        compositeDisposable
                .add(interactor.firebaseRegistration(mAuth, s, s1)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> authorizationActivity.showToast("Registration passed"),
                                throwable -> authorizationActivity.showToast(throwable.getMessage())));
    }
}
