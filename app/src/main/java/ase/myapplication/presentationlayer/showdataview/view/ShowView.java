package ase.myapplication.presentationlayer.showdataview.view;

public interface ShowView {

    void showToast(String s);
    void showProgressBar();
}
