package ase.myapplication.presentationlayer.showdataview.presenter;

import java.util.List;

import javax.inject.Inject;

import ase.myapplication.busineslayer.showview.showinteractor.ShowInteractor;
import ase.myapplication.datalayer.InputDataModel;
import ase.myapplication.presentationlayer.showdataview.view.ShowActivity;
import dagger.Reusable;
import io.reactivex.disposables.CompositeDisposable;

@Reusable
public class ShowPresenter {

    private ShowInteractor showInteractor;
    private ShowActivity showActivity;
    private CompositeDisposable compositeDisposable  = new CompositeDisposable();

    @Inject
    ShowPresenter(ShowInteractor showInteractor) {
        this.showInteractor = showInteractor;
    }

    public void attachView(ShowActivity showActivity) {
        this.showActivity = showActivity;
    }

    public void detachView() {
        compositeDisposable.clear();
        showActivity = null;
    }

    public void getDataToShow() {
        showActivity.showProgressBar();
        List<InputDataModel> inputDataModels = showInteractor.getDataFromFB();

        showActivity.hideProgressBar();
        showActivity.updateActivity(inputDataModels);
    }
}
