package ase.myapplication.presentationlayer.showdataview.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ase.myapplication.App;
import ase.myapplication.R;
import ase.myapplication.datalayer.DataViewAdapter;
import ase.myapplication.datalayer.InputDataModel;
import ase.myapplication.presentationlayer.showdataview.presenter.ShowPresenter;

public class ShowActivity extends AppCompatActivity  {

    @Inject
    ShowPresenter presenter;
    RecyclerView recyclerView;
    List<InputDataModel> inputDataModels;
    DataViewAdapter adapter;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        inputDataModels = new ArrayList<>();
        progressBar = findViewById(R.id.progressBar2);

//        updateActivity(inputDataModels);
        App.getComponent().inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
        presenter.getDataToShow();
    }

    @Override
    protected void onStop() {
        presenter.detachView();
        super.onStop();
    }

    public void updateActivity(List<InputDataModel> inputDataModels) {
        recyclerView = findViewById(R.id.datetextlist);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);
        adapter = new DataViewAdapter(inputDataModels);
        recyclerView.setAdapter(adapter);
    }


    public void showProgressBar() {
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(ProgressBar.INVISIBLE);
    }
}
