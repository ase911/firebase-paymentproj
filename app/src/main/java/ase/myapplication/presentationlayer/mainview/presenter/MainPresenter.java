package ase.myapplication.presentationlayer.mainview.presenter;

import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

import ase.myapplication.busineslayer.mainview.interactor.MainInteractorImpl;
import ase.myapplication.presentationlayer.mainview.view.MainActivity;
import dagger.Reusable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

@Reusable
public class MainPresenter {

    private MainInteractorImpl interactor;
    private MainActivity mainActivity;
    private CompositeDisposable compositeDisposable  = new CompositeDisposable();

    @Inject
    MainPresenter(MainInteractorImpl interactor) {
        this.interactor = interactor;
    }

    public void attachView(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void detachView() {
        compositeDisposable.clear();
        mainActivity = null;
    }

    public void signOut() {
        interactor.FirebaseSignOut();
    }

    public void verifyEmail(FirebaseUser currentUser) {
        compositeDisposable
                .add(interactor
                        .VerifyEmail(currentUser)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() ->
                                        mainActivity.showToast("Verification message was send on your email"),
                                throwable ->
                                        mainActivity.showToast(throwable.getMessage())));
    }
}
