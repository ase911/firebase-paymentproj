package ase.myapplication.presentationlayer.mainview.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

import ase.myapplication.App;
import ase.myapplication.R;
import ase.myapplication.presentationlayer.authorizationview.view.AuthorizationActivity;
import ase.myapplication.presentationlayer.mainview.presenter.MainPresenter;
import ase.myapplication.presentationlayer.senddataview.view.SendDataActivity;
import ase.myapplication.presentationlayer.showdataview.view.ShowActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "MainActivity";

    private AdView mAdView;
    @Inject
    protected MainPresenter presenter;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private TextView loginTextView;
    private TextView verifyTextView;
    private Button verifyButton;
    private Button sendButton;
    private Button getDataButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginTextView = findViewById(R.id.textView);
        verifyTextView = findViewById(R.id.textView2);

        Button exitButton = findViewById(R.id.sign_out_button);
        sendButton = findViewById(R.id.writeActivityButton);
        verifyButton = findViewById(R.id.verificationButton);
        getDataButton = findViewById(R.id.readActivityButton);

        exitButton.setOnClickListener(this);
        sendButton.setOnClickListener(this);
        getDataButton.setOnClickListener(this);
        verifyButton.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();

//        mAdView = (AdView) findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                .addTestDevice("A9F28E24332BDC37DBA04B46BC0E7728")
//                .build();
//        mAdView.loadAd(adRequest);

        App.getComponent().inject(this);

    }


    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
        currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    public void updateUI(FirebaseUser currentUser) {
        loginTextView.setText(currentUser.getEmail());
        verifyTextView.setText(String.format("Verification: %s", currentUser.isEmailVerified()));
        if (currentUser.isEmailVerified()){
            verifyButton.setVisibility(View.GONE);
        } else {
            sendButton.setVisibility(View.GONE);
            getDataButton.setVisibility(View.GONE);
        }
    }

    public void showToast(String ae) {
        Toast.makeText(this, ae, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (v.getId()) {
            case  R.id.sign_out_button: {
                presenter.signOut();
                Intent intent = new Intent(this, AuthorizationActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case R.id.writeActivityButton: {
                Intent intent = new Intent(this, SendDataActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.readActivityButton:{
                Intent intent = new Intent(this, ShowActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.verificationButton: {
                presenter.verifyEmail(currentUser);
                break;
            }
        }
    }

    @Override
    protected void onStop() {
        presenter.detachView();
        super.onStop();
    }
}
