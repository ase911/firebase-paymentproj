package ase.myapplication.presentationlayer.senddataview.presenter;

import ase.myapplication.busineslayer.senddataview.interactor.SendDataInteractor;
import ase.myapplication.busineslayer.senddataview.interactor.SendDataInteractorImpl;

public class SendDataPresenter {

    private SendDataInteractor interactor = new SendDataInteractorImpl();

    public void putIntoDB(String s, String s1) {
        interactor.putIntoDB(s, s1);
    }

}
