package ase.myapplication.presentationlayer.senddataview.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import ase.myapplication.R;
import ase.myapplication.presentationlayer.senddataview.presenter.SendDataPresenter;

public class SendDataActivity extends AppCompatActivity implements View.OnClickListener {

    SendDataPresenter presenter = new SendDataPresenter();
    TextView sumTextView;
    TextView commentTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        sumTextView = findViewById(R.id.inputsum);
        commentTextView = findViewById(R.id.inputcomment);
        //TODO: добавить выпадающее меню
        findViewById(R.id.writeButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.writeButton) {
            presenter.putIntoDB(sumTextView.getText().toString(),
                    commentTextView.getText().toString());
        }
        sumTextView.setText("");
        commentTextView.setText("");
        Toast.makeText(this, "Данные записаны", Toast.LENGTH_SHORT).show();
    }
}
