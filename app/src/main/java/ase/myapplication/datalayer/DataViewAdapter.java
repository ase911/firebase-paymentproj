package ase.myapplication.datalayer;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ase.myapplication.R;

public class DataViewAdapter extends RecyclerView.Adapter<DataViewAdapter.DataViewHolder>{

    private List<InputDataModel> inputDataModels;

    public DataViewAdapter(List<InputDataModel> inputDataModels) {
        this.inputDataModels = inputDataModels;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater
                .from(parent
                        .getContext())
                .inflate(R.layout.item_data, parent, false));
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {

        InputDataModel inputDataModel = inputDataModels.get(position);

        holder.dateTextView.setText(inputDataModel.date);
        holder.sumTextView.setText(inputDataModel.sum);
        holder.commentTextView.setText(inputDataModel.comment);
        boolean b = Boolean.parseBoolean(inputDataModel.type);
        if (b){
            holder.imageView.setBackgroundColor(Color.RED);
        } else {
            holder.imageView.setBackgroundColor(Color.GREEN);
        }
    }

    @Override
    public int getItemCount() {
        return inputDataModels.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {

        TextView sumTextView;
        TextView dateTextView;
        TextView commentTextView;
        ImageView imageView;

        DataViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            sumTextView = itemView.findViewById(R.id.sumtext);
            dateTextView = itemView.findViewById(R.id.datetext);
            commentTextView = itemView.findViewById(R.id.comment_textview);
        }
    }


}
