package ase.myapplication.datalayer;

public class InputDataModel {

    String sum;
    String date;
    String type;
    String comment;

    public InputDataModel() {
    }

    public InputDataModel(String sum, String date, String type, String comment) {
        this.sum = sum;
        this.date = date;
        this.type = type;
        this.comment = comment;

    }
}
