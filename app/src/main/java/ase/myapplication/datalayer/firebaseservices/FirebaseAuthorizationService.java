package ase.myapplication.datalayer.firebaseservices;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

import io.reactivex.Completable;

public class FirebaseAuthorizationService {

    @Inject
    FirebaseAuthorizationService(){
    }

    public void signOut(){
        FirebaseAuth.getInstance().signOut();
    }

    public Completable signIn(final FirebaseAuth auth, final String login, final String password){
        return Completable
                .create(e -> auth.signInWithEmailAndPassword(login,password)
                        .addOnFailureListener(e::onError)
                        .addOnSuccessListener(runnable -> e.onComplete()));
    }

    public Completable registration(FirebaseAuth auth, String login, String password) {
        return Completable
                .create(e ->
                        auth.createUserWithEmailAndPassword(login, password)
                                .addOnFailureListener(e::onError)
                                .addOnSuccessListener(runnable -> e.onComplete()));
    }


    public Completable verification(FirebaseUser user) {
        return Completable
                .create(e -> user
                        .sendEmailVerification()
                .addOnFailureListener(e::onError)
                .addOnSuccessListener(runnable -> e.onComplete()));
    }
}
