package ase.myapplication.datalayer.firebaseservices;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ase.myapplication.datalayer.InputDataModel;

public class RealtimeDatabaseService {

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef;

    @Inject
    public RealtimeDatabaseService() {
    }

    public void putIntoDB(String s, String s1){

        long date = System.currentTimeMillis();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd_HH:mm:ss");
        String dateString = sdf.format(date);

        myRef = database.getReference("users")
                        .child(FirebaseAuth
                                .getInstance()
                                .getCurrentUser()
                                .getUid())
                        .child(dateString);
        InputDataModel inputDataModel = new InputDataModel(s+" p", dateString, "true", s1);
        myRef.setValue(inputDataModel);
    }

    public List<InputDataModel> getDataForFB(){

        // TODO: 14.11.2017 переписать на observeble 
        List<InputDataModel> inputDataModels = new ArrayList<>();
        myRef = database.getReference("users")
                        .child(FirebaseAuth
                                .getInstance()
                                .getCurrentUser()
                                .getUid());

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String str = dataSnapshot.getKey();

                Log.d("Pay", " "+dataSnapshot.getValue(InputDataModel.class));

                inputDataModels.add(dataSnapshot.getValue(InputDataModel.class));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return inputDataModels;
    }


}
